var express = require("express.io");
var five = require("johnny-five");
var app = express();
var io = app.http().io();
var led;
var arduino = new five.Board();
arduino.on("ready", function() {
  led = new five.Led(11);
});

app.io.route('encender', function(){
  led.fadeIn(3000);
});
app.io.route('apagar', function(){
  led.fadeOut(1200);
});
app.get('/', function(req, res) {
    res.sendfile(__dirname + '/index.html')
});
app.listen(80, function(){
  console.log("Servidor Encendido");
});
